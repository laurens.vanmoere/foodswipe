"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("firebase/app");
const database_1 = require("firebase/database");
const storage_1 = require("firebase/storage");
const auth_1 = require("firebase/auth");
const firebaseConfig = {
    apiKey: "AIzaSyAx_J5kpKeSgMjfZZ4M5_bdPcfp6KPl75E",
    authDomain: "foodswipe-35b2a.firebaseapp.com",
    databaseURL: "https://foodswipe-35b2a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "foodswipe-35b2a",
    storageBucket: "foodswipe-35b2a.appspot.com",
    messagingSenderId: "51231404668",
    appId: "1:51231404668:web:0ccdc35c8dff89a3a28ff9",
    measurementId: "G-X6MMWS4ZRV"
};
var signInBtn = document.querySelector("#SignInBtn");
if (signInBtn != null)
    signInBtn.innerText = getCookie('name');
(_a = document.querySelector("#SignInBtn")) === null || _a === void 0 ? void 0 : _a.addEventListener("click", openProfileForm);
(_b = document.querySelector("#signOutBtn")) === null || _b === void 0 ? void 0 : _b.addEventListener("click", signOutAccount);
(_c = document.querySelector("#CloseProfile")) === null || _c === void 0 ? void 0 : _c.addEventListener("click", closeProfileForm);
(0, app_1.initializeApp)(firebaseConfig);
getFriends();
function getFriends() {
    return __awaiter(this, void 0, void 0, function* () {
        const db = (0, database_1.getDatabase)();
        const storage = (0, storage_1.getStorage)();
        let listOfLikedRecipesIDs = [];
        const friendsList = document.querySelector('.friendsList');
        if (friendsList != null)
            friendsList.innerHTML = "";
        yield (0, database_1.onValue)((0, database_1.ref)(db, 'users/' + getCookie('id') + '/friends'), (friends) => {
            friends.forEach((friend) => {
                (0, database_1.onValue)((0, database_1.ref)(db, 'users/' + friend.key), (user) => {
                    if (friendsList != null) {
                        friendsList.innerHTML +=
                            `<friends-intro image="../images/profileDummy.png">
                        <span id="PersonName" slot="PersonName">${user.val().name}</span>
                    </friends-intro>`;
                    }
                });
            });
        });
    });
}
function getCookie(name) {
    let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : "";
}
function openProfileForm() {
    (0, database_1.onValue)((0, database_1.ref)((0, database_1.getDatabase)(), `users/${getCookie('id')}`), (snapshot) => {
        const nameBox = document.querySelector('#displayNameBox');
        if (nameBox != null)
            nameBox.innerText = getCookie('name');
        const emailBox = document.getElementById('emailBox');
        if (emailBox != null)
            emailBox.innerText = snapshot.val().email;
    });
    const profileForm = document.querySelector('#ProfileForm');
    if (profileForm != null)
        profileForm.style.display = 'block';
}
function signOutAccount() {
    return __awaiter(this, void 0, void 0, function* () {
        (0, auth_1.getAuth)().signOut();
        closeProfileForm();
        const btn = document.querySelector('#SignInBtn');
        if (btn != null)
            btn.innerText = 'Sign in';
        window.location.href = '../index.html';
    });
}
function closeProfileForm() {
    const profileForm = document.querySelector('#ProfileForm');
    if (profileForm != null)
        profileForm.style.display = 'none';
}
//# sourceMappingURL=friends.js.map