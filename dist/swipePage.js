"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f;
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("firebase/app");
const database_1 = require("firebase/database");
const storage_1 = require("firebase/storage");
const auth_1 = require("firebase/auth");
const firebaseConfig = {
    apiKey: "AIzaSyAx_J5kpKeSgMjfZZ4M5_bdPcfp6KPl75E",
    authDomain: "foodswipe-35b2a.firebaseapp.com",
    databaseURL: "https://foodswipe-35b2a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "foodswipe-35b2a",
    storageBucket: "foodswipe-35b2a.appspot.com",
    messagingSenderId: "51231404668",
    appId: "1:51231404668:web:0ccdc35c8dff89a3a28ff9",
    measurementId: "G-X6MMWS4ZRV"
};
(0, app_1.initializeApp)(firebaseConfig);
var recipeCounter = 1;
(_a = document.querySelector("#AddBtn")) === null || _a === void 0 ? void 0 : _a.addEventListener("click", AddRecipe);
(_b = document.querySelector("#ReturnBtn")) === null || _b === void 0 ? void 0 : _b.addEventListener("click", getLastRecipe);
(_c = document.querySelector("#ThrowAwayBtn")) === null || _c === void 0 ? void 0 : _c.addEventListener("click", ThrowAwayRecipe);
var signInBtn = document.querySelector("#SignInBtn");
if (signInBtn != null)
    signInBtn.innerText = getCookie('name');
(_d = document.querySelector("#SignInBtn")) === null || _d === void 0 ? void 0 : _d.addEventListener("click", openProfileForm);
(_e = document.querySelector("#signOutBtn")) === null || _e === void 0 ? void 0 : _e.addEventListener("click", signOutAccount);
(_f = document.querySelector("#CloseProfile")) === null || _f === void 0 ? void 0 : _f.addEventListener("click", closeProfileForm);
let userID = getCookie('id');
getRecipe();
function getCookie(name) {
    let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : "";
}
function ThrowAwayRecipe() {
    return __awaiter(this, void 0, void 0, function* () {
        (0, database_1.set)((0, database_1.ref)((0, database_1.getDatabase)(), 'users/' + userID + '/recipeCount/'), {
            Count: recipeCounter + 1
        });
        recipeCounter++;
        getRecipe();
    });
}
function getRecipe() {
    return __awaiter(this, void 0, void 0, function* () {
        let recipeCount = 1;
        yield (0, database_1.onValue)((0, database_1.ref)((0, database_1.getDatabase)(), `users/` + userID), (snapshot) => {
            recipeCount = snapshot.val().recipeCount['Count'];
            getRecipeByNumber(recipeCount);
        });
    });
}
function getRecipeByNumber(recipeNumber) {
    return __awaiter(this, void 0, void 0, function* () {
        const title = document.getElementById('Title');
        const shortText = document.getElementById('ShortText');
        const cookingDifficulty = document.getElementById('CookingDifficulty');
        const cookingTime = document.getElementById('CookingTime');
        const cookingServes = document.getElementById('CookingServes');
        yield (0, database_1.onValue)((0, database_1.ref)((0, database_1.getDatabase)(), `recipes/${recipeNumber}`), (snapshot) => {
            if (title != null)
                title.innerText = snapshot.val().title;
            if (shortText != null)
                shortText.innerText = snapshot.val().shortText;
            if (cookingDifficulty != null)
                cookingDifficulty.innerText = snapshot.val().CookingDifficulty;
            if (cookingTime != null)
                cookingTime.innerText = snapshot.val().CookingTime;
            if (cookingServes != null)
                cookingServes.innerText = snapshot.val().CookingServes;
            const storage = (0, storage_1.getStorage)();
            (0, storage_1.getDownloadURL)((0, storage_1.ref)(storage, 'recipeImages/' + snapshot.val().Image)).then((url) => {
                var _a;
                (_a = document.querySelector('recipe-intro')) === null || _a === void 0 ? void 0 : _a.setAttribute('image', url);
            });
        });
    });
}
function getLastRecipe() {
    return __awaiter(this, void 0, void 0, function* () {
        yield (0, database_1.set)((0, database_1.ref)((0, database_1.getDatabase)(), 'users/' + userID + '/recipeCount/'), {
            Count: recipeCounter - 1
        });
        getRecipe();
        var auth = (0, auth_1.getAuth)();
        const user = auth.currentUser;
    });
}
function openProfileForm() {
    (0, database_1.onValue)((0, database_1.ref)((0, database_1.getDatabase)(), `users/${getCookie('id')}`), (snapshot) => {
        const nameBox = document.querySelector('#displayNameBox');
        if (nameBox != null)
            nameBox.innerText = getCookie('name');
        const emailBox = document.getElementById('emailBox');
        if (emailBox != null)
            emailBox.innerText = snapshot.val().email;
    });
    const profileForm = document.querySelector('#ProfileForm');
    if (profileForm != null)
        profileForm.style.display = 'block';
}
function signOutAccount() {
    return __awaiter(this, void 0, void 0, function* () {
        (0, auth_1.getAuth)().signOut();
        closeProfileForm();
        const btn = document.querySelector('#SignInBtn');
        if (btn != null)
            btn.innerText = 'Sign in';
        window.location.href = '../index.html';
    });
}
function closeProfileForm() {
    const profileForm = document.querySelector('#ProfileForm');
    if (profileForm != null)
        profileForm.style.display = 'none';
}
function AddRecipe() {
    return __awaiter(this, void 0, void 0, function* () {
        const db = (0, database_1.getDatabase)();
        yield (0, database_1.set)((0, database_1.ref)(db, 'users/' + getCookie('id') + '/liked/' + recipeCounter), {
            dateAdded: new Date().toLocaleString()
        });
        (0, database_1.set)((0, database_1.ref)((0, database_1.getDatabase)(), 'users/' + userID + '/recipeCount/'), {
            Count: recipeCounter + 1
        });
        CheckLikedRecipesOtherPeople();
        recipeCounter++;
        getRecipe();
    });
}
function CheckLikedRecipesOtherPeople() {
    return __awaiter(this, void 0, void 0, function* () {
        if (recipeCounter === 5) {
            yield AddFriends();
        }
    });
}
function AddFriends() {
    return __awaiter(this, void 0, void 0, function* () {
        const db = (0, database_1.getDatabase)();
        const storage = (0, storage_1.getStorage)();
        console.log("helllloooo");
        let listOf3Users = [];
        yield (0, database_1.onValue)((0, database_1.ref)(db, 'users/'), (AllUsers) => {
            let friendCounter = 0;
            AllUsers.forEach((user) => {
                if (friendCounter < 3) {
                    if (user.key != getCookie('id')) {
                        let id = user.key;
                        listOf3Users.push(id);
                        friendCounter++;
                    }
                }
                if (listOf3Users.length == 3) {
                    for (let i = 0; i < listOf3Users.length; i++) {
                        (0, database_1.set)((0, database_1.ref)((0, database_1.getDatabase)(), 'users/' + getCookie('id') + '/friends/' + listOf3Users[i]), {
                            TimeAdded: time
                        });
                    }
                }
            });
        });
        const d = new Date();
        let time = d.getTime();
    });
}
//# sourceMappingURL=swipePage.js.map