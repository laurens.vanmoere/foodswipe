"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g, _h;
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("firebase/app");
const auth_1 = require("firebase/auth");
const database_1 = require("firebase/database");
const firebaseConfig = {
    apiKey: "AIzaSyAx_J5kpKeSgMjfZZ4M5_bdPcfp6KPl75E",
    authDomain: "foodswipe-35b2a.firebaseapp.com",
    databaseURL: "https://foodswipe-35b2a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "foodswipe-35b2a",
    storageBucket: "foodswipe-35b2a.appspot.com",
    messagingSenderId: "51231404668",
    appId: "1:51231404668:web:0ccdc35c8dff89a3a28ff9",
    measurementId: "G-X6MMWS4ZRV"
};
(0, app_1.initializeApp)(firebaseConfig);
(_a = document.querySelector("#MakeAccountBtn")) === null || _a === void 0 ? void 0 : _a.addEventListener("click", openRegisterForm);
(_b = document.querySelector("#closeFrom")) === null || _b === void 0 ? void 0 : _b.addEventListener("click", closeRegisterForm);
(_c = document.querySelector("#SignInBtn")) === null || _c === void 0 ? void 0 : _c.addEventListener("click", openSignInForm);
(_d = document.querySelector("#CloseProfile")) === null || _d === void 0 ? void 0 : _d.addEventListener("click", closeProfileForm);
(_e = document.querySelector("#CloseSignIn")) === null || _e === void 0 ? void 0 : _e.addEventListener("click", closeSignInForm);
(_f = document.querySelector('#LoginBtn')) === null || _f === void 0 ? void 0 : _f.addEventListener("click", makeAccount);
(_g = document.querySelector('#SignInBtnForm')) === null || _g === void 0 ? void 0 : _g.addEventListener("click", signInAccount);
(_h = document.querySelector('#signOutBtn')) === null || _h === void 0 ? void 0 : _h.addEventListener("click", signOutAccount);
function makeAccount() {
    return __awaiter(this, void 0, void 0, function* () {
        const emailBox = document.getElementById('email');
        let email = "";
        if (emailBox != null)
            email = emailBox === null || emailBox === void 0 ? void 0 : emailBox.value;
        const password1Box = document.getElementById('password1');
        let password1 = "";
        if (password1Box != null)
            password1 = password1Box.value;
        const password2Box = document.getElementById('password2');
        let password2 = "";
        if (password2Box != null)
            password2 = password2Box.value;
        if (password1 == password2) {
            var auth = (0, auth_1.getAuth)();
            yield (0, auth_1.createUserWithEmailAndPassword)(auth, email, password1).then(() => closeRegisterForm());
            auth = (0, auth_1.getAuth)();
            if (auth.currentUser != null) {
                let nameBox = document.getElementById('name');
                let user = auth.currentUser;
                if (nameBox != null)
                    yield (0, auth_1.updateProfile)(user, { displayName: nameBox.value });
            }
            auth = (0, auth_1.getAuth)();
            var user = auth.currentUser;
            if (user != null) {
                yield user.providerData.forEach((profile) => {
                    let signInBtn = document.getElementById('SignInBtn');
                    if (signInBtn != null)
                        signInBtn.innerText = String(profile.displayName);
                });
                yield (0, database_1.set)((0, database_1.ref)((0, database_1.getDatabase)(), 'users/' + user.uid.toString()), {
                    email: user.email,
                    recipeCount: { Count: 1 },
                    name: user.displayName
                });
                setCookie('id', user.uid.toString(), '/');
                setCookie('name', String(user.displayName), '/');
            }
            if (getCookie('id') && getCookie('name')) {
                window.location.href = '../swipePage.html';
            }
        }
        else {
            console.log("Pasword mismatch");
            // Passwords dont match
        }
    });
}
function signInAccount() {
    return __awaiter(this, void 0, void 0, function* () {
        const emailBox = document.getElementById('emailSignIn');
        let email = "";
        if (emailBox != null)
            email = emailBox === null || emailBox === void 0 ? void 0 : emailBox.value;
        const passwordBox = document.getElementById('passwordSignIn');
        let password = "";
        if (passwordBox != null)
            password = passwordBox.value;
        var auth = (0, auth_1.getAuth)();
        yield (0, auth_1.signInWithEmailAndPassword)(auth, email, password);
        closeSignInForm();
        let user = auth.currentUser;
        let userId = "";
        let userName = "";
        if (user != null) {
            userId = String(user.uid);
            userName = String(user.displayName);
        }
        setCookie('id', userId, '/');
        setCookie('name', userName, '/');
        if (getCookie('id') && getCookie('name')) {
            window.location.href = '../swipePage.html';
        }
    });
}
function setCookie(name, value, path, options = {}) {
    options = Object.assign({ path: path }, options);
    /*if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    } */
    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);
    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }
    document.cookie = updatedCookie;
}
function getCookie(name) {
    let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : "";
}
function openSignInForm() {
    var auth = (0, auth_1.getAuth)();
    const user = auth.currentUser;
    if (user == null) {
        const signinForm = document.querySelector('#SignInForm');
        if (signinForm != null)
            signinForm.style.display = 'block';
        document.querySelectorAll("div:not(#SignInForm)").forEach(element => {
            if (element.className !== 'ColorStroke' && element.className !== 'loginLinks' && element.className !== 'links') {
                const elementvar = element;
                if (elementvar != null)
                    elementvar.style.filter = 'brightness(70%)';
            }
        });
    }
    else {
        const profileForm = document.querySelector('#ProfileForm');
        if (profileForm != null)
            profileForm.style.display = 'block';
        const nameBox = document.querySelector('#displayNameBox');
        if (nameBox != null)
            nameBox.innerText = String(user.displayName);
        const emailBox = document.getElementById('emailBox');
        if (emailBox != null)
            emailBox.innerText = String(user.email);
        document.querySelectorAll("div:not(#ProfileForm)").forEach(element => {
            if (element.className !== 'ColorStroke' && element.className !== 'loginLinks' && element.className !== 'links') {
                const elementvar = element;
                if (elementvar != null)
                    elementvar.style.filter = 'brightness(70%)';
            }
        });
    }
}
function signOutAccount() {
    return __awaiter(this, void 0, void 0, function* () {
        (0, auth_1.getAuth)().signOut();
        closeProfileForm();
        const btn = document.querySelector('#SignInBtn');
        if (btn != null)
            btn.innerText = 'Sign in';
    });
}
function closeProfileForm() {
    const profileForm = document.querySelector('#ProfileForm');
    if (profileForm != null)
        profileForm.style.display = 'none';
    document.querySelectorAll("div").forEach(element => {
        element.style.filter = 'brightness(100%)';
    });
}
function closeSignInForm() {
    const signinForm = document.querySelector('#SignInForm');
    if (signinForm != null)
        signinForm.style.display = 'none';
    document.querySelectorAll("div").forEach(element => {
        element.style.filter = 'brightness(100%)';
    });
}
function openRegisterForm() {
    const registerForm = document.querySelector('#RegisterForm');
    if (registerForm != null)
        registerForm.style.display = 'block';
    document.querySelectorAll("div:not(#RegisterForm)").forEach((element) => {
        if (element.className !== 'ColorStroke' && element.className !== 'loginLinks' && element.className !== 'links') {
            const elementvar = element;
            if (elementvar != null)
                elementvar.style.filter = 'brightness(70%)';
        }
    });
}
function closeRegisterForm() {
    const registerForm = document.querySelector('#RegisterForm');
    if (registerForm != null)
        registerForm.style.display = 'none';
    document.querySelectorAll("div").forEach(element => {
        element.style.filter = 'brightness(100%)';
    });
}
//# sourceMappingURL=MakeAccount.js.map