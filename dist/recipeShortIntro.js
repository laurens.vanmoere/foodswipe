"use strict";
const recipeShortIntro = document.createElement('template');
recipeShortIntro.innerHTML = `
<style>
.listElement
{
    display:flex; 
    width:60%; 
    align-items: center; 
    margin: auto; 
    text-align: left; 
    border: 3px solid var(--main-dark-color); 
    border-radius: 5px;

    margin-bottom: 10px;
}

img
{
    width: 150px; 
    height: 150px; 
    margin: 10px; 
    border-radius: 5px;
}

button
{
    border-radius: 30px;
    height: 40px;
    background-color: var(--main-dark-color); 
    padding: 5px;

    width: 300px;
    cursor: pointer;
    border: none;
    color:white;
    margin: 20px;;
}
</style>

<div class="listElement">
    <img>
    <div style="margin: 5px; width = 50%">
        <h2><slot name="RecipeName"/>RecipeName</h2>
        <p><slot name="ShortText"/></p>
        
    </div>

    <button>View Recipe</button>
</div>
`;
class RecipeShortIntro extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        if (this.shadowRoot != null)
            this.shadowRoot.appendChild(recipeShortIntro.content.cloneNode(true));
    }
    static get observedAttributes() {
        return ['image'];
    }
    attributeChangedCallback() {
        if (this.shadowRoot != null) {
            const image = this.shadowRoot.querySelector('img');
            const imageAttr = this.getAttribute('image');
            if (image != null && image.src != null && imageAttr != null) {
                image.src = imageAttr;
            }
        }
    }
}
customElements.define('recipe-short-intro', RecipeShortIntro);
//# sourceMappingURL=recipeShortIntro.js.map