"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g;
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("firebase/app");
const database_1 = require("firebase/database");
const storage_1 = require("firebase/storage");
const auth_1 = require("firebase/auth");
const gsap_1 = require("gsap");
const firebaseConfig = {
    apiKey: "AIzaSyAx_J5kpKeSgMjfZZ4M5_bdPcfp6KPl75E",
    authDomain: "foodswipe-35b2a.firebaseapp.com",
    databaseURL: "https://foodswipe-35b2a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "foodswipe-35b2a",
    storageBucket: "foodswipe-35b2a.appspot.com",
    messagingSenderId: "51231404668",
    appId: "1:51231404668:web:0ccdc35c8dff89a3a28ff9",
    measurementId: "G-X6MMWS4ZRV"
};
(_a = document.querySelector(".buttonFilter")) === null || _a === void 0 ? void 0 : _a.addEventListener('mouseenter', () => gsap_1.gsap.to(".buttonFilter", { duration: 2, backgroundColor: "#143d59", color: "white" }));
(_b = document.querySelector(".buttonFilter")) === null || _b === void 0 ? void 0 : _b.addEventListener('mouseleave', () => gsap_1.gsap.to(".buttonFilter", { duration: 2, backgroundColor: "white", color: "#143d59" }));
(_c = document.querySelector(".buttonFilter")) === null || _c === void 0 ? void 0 : _c.addEventListener('click', getAllLikedRecipes);
(_d = document.querySelector(".DeleteInputFields")) === null || _d === void 0 ? void 0 : _d.addEventListener('click', clearAllInput);
var signInBtn = document.querySelector("#SignInBtn");
if (signInBtn != null)
    signInBtn.innerText = getCookie('name');
(_e = document.querySelector("#SignInBtn")) === null || _e === void 0 ? void 0 : _e.addEventListener("click", openProfileForm);
(_f = document.querySelector("#signOutBtn")) === null || _f === void 0 ? void 0 : _f.addEventListener("click", signOutAccount);
(_g = document.querySelector("#CloseProfile")) === null || _g === void 0 ? void 0 : _g.addEventListener("click", closeProfileForm);
(0, app_1.initializeApp)(firebaseConfig);
getAllLikedRecipes();
function clearAllInput() {
    let nameValue = document.getElementsByClassName("inputFilter")[0];
    if (nameValue != null)
        nameValue.value = "";
    let difficultyValue = document.getElementsByClassName("inputFilter")[1];
    if (difficultyValue != null)
        difficultyValue.value = "";
    let timeValue = document.getElementsByClassName("inputFilter")[2];
    if (timeValue != null)
        timeValue.value = "";
}
function getAllLikedRecipes() {
    return __awaiter(this, void 0, void 0, function* () {
        const db = (0, database_1.getDatabase)();
        const storage = (0, storage_1.getStorage)();
        let listOfLikedRecipesIDs = [];
        yield (0, database_1.onValue)((0, database_1.ref)(db, 'users/' + getCookie('id') + '/liked'), (liked) => {
            liked.forEach((likedRecipes) => { let id = likedRecipes.key; listOfLikedRecipesIDs.push(id); });
            selectAllRecipes(listOfLikedRecipesIDs);
        });
    });
}
function selectAllRecipes(listOfLikedRecipesIDs) {
    return __awaiter(this, void 0, void 0, function* () {
        const db = (0, database_1.getDatabase)();
        const storage = (0, storage_1.getStorage)();
        let nameValue = document.getElementsByClassName("inputFilter")[0];
        let name = "";
        if (nameValue != null)
            name = String(nameValue.value);
        let difficultyValue = document.getElementsByClassName("inputFilter")[1];
        let difficulty = "";
        if (difficultyValue != null)
            difficulty = String(difficultyValue.value);
        let timeValue = document.getElementsByClassName("inputFilter")[2];
        let time = "";
        if (timeValue != null)
            time = String(timeValue.value);
        //var name = String(document.getElementsByClassName("inputFilter")[0].value);
        //var difficulty = String(document.getElementsByClassName("inputFilter")[1].value);
        //var time = String(document.getElementsByClassName("inputFilter")[2].value);
        let recipeList = document.querySelector('.recipeList');
        if (recipeList != null)
            recipeList.innerHTML = "";
        for (let i = 0; i < listOfLikedRecipesIDs.length; i++) {
            yield (0, database_1.onValue)((0, database_1.ref)(db, 'recipes/' + listOfLikedRecipesIDs[i]), (recipe) => {
                if (name.length > 0 || time.length > 0 || difficulty.length > 0) {
                    if (name.length == 0)
                        name = '/';
                    if (time.length == 0)
                        time = '/';
                    if (difficulty.length == 0)
                        difficulty = '/';
                    if (recipe.val().title.includes(name) || recipe.val().CookingTime == (time + " min") || recipe.val().CookingDifficulty.includes(difficulty)) {
                        (0, storage_1.getDownloadURL)((0, storage_1.ref)(storage, 'recipeImages/' + recipe.val().Image)).then((url) => {
                            if (recipeList != null) {
                                recipeList.innerHTML +=
                                    `<recipe-short-intro image="${url}" id="${listOfLikedRecipesIDs[i]}">
                       <span id="RecipeName" slot="RecipeName">${recipe.val().title}</span>
                       <span id="ShortText" slot="ShortText">${recipe.val().shortText}</span>
                   </recipe-short-intro>`;
                            }
                        });
                    }
                }
                else {
                    (0, storage_1.getDownloadURL)((0, storage_1.ref)(storage, 'recipeImages/' + recipe.val().Image)).then((url) => {
                        if (recipeList != null) {
                            recipeList.innerHTML +=
                                `<recipe-short-intro image="${url}" id="${listOfLikedRecipesIDs[i]}">
                    <span id="RecipeName" slot="RecipeName">${recipe.val().title}</span>
                    <span id="ShortText" slot="ShortText">${recipe.val().shortText}</span>
                </recipe-short-intro>`;
                        }
                    });
                }
            });
        }
    });
}
function getCookie(name) {
    let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : "";
}
function openProfileForm() {
    (0, database_1.onValue)((0, database_1.ref)((0, database_1.getDatabase)(), `users/${getCookie('id')}`), (snapshot) => {
        const nameBox = document.querySelector('#displayNameBox');
        if (nameBox != null)
            nameBox.innerText = getCookie('name');
        const emailBox = document.getElementById('emailBox');
        if (emailBox != null)
            emailBox.innerText = snapshot.val().email;
    });
    const profileForm = document.querySelector('#ProfileForm');
    if (profileForm != null)
        profileForm.style.display = 'block';
}
function signOutAccount() {
    return __awaiter(this, void 0, void 0, function* () {
        (0, auth_1.getAuth)().signOut();
        closeProfileForm();
        const btn = document.querySelector('#SignInBtn');
        if (btn != null)
            btn.innerText = 'Sign in';
        window.location.href = '../index.html';
    });
}
function closeProfileForm() {
    const profileForm = document.querySelector('#ProfileForm');
    if (profileForm != null)
        profileForm.style.display = 'none';
}
//# sourceMappingURL=recipes.js.map