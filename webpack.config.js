const path = require('path')

module.exports = {
    mode: 'development',
    entry: './dist/friends.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundleFriends.js'
    },
    watch: true
}
  
module.exports = {
    mode: 'development',
    entry: './dist/rssFeed.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundleRssFeed.js'
    },
    watch: true
}


module.exports = {
  mode: 'development',
  entry: './dist/recipes.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundleRecipes.js'
  },
  watch: true
}


module.exports = {
  mode: 'development',
  entry: './dist/MakeAccount.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundleMakeAccount.js'
  },
  watch: true
}

module.exports = {
  mode: 'development',
  entry: './dist/swipePage.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundleSwipePage.js'
  },
  watch: true
}