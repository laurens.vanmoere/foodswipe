import { initializeApp } from 'firebase/app';
import { getDatabase, ref, query, onValue } from "firebase/database";
import { ref as sRef, getDownloadURL, getStorage, list  } from 'firebase/storage';
import { getAuth, createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword } from "firebase/auth";
import { gsap } from 'gsap';

const firebaseConfig = {
    apiKey: "AIzaSyAx_J5kpKeSgMjfZZ4M5_bdPcfp6KPl75E",
    authDomain: "foodswipe-35b2a.firebaseapp.com",
    databaseURL: "https://foodswipe-35b2a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "foodswipe-35b2a",
    storageBucket: "foodswipe-35b2a.appspot.com",
    messagingSenderId: "51231404668",
    appId: "1:51231404668:web:0ccdc35c8dff89a3a28ff9",
    measurementId: "G-X6MMWS4ZRV"
};

document.querySelector(".buttonFilter")?.addEventListener('mouseenter', () => gsap.to(".buttonFilter", {duration: 2,backgroundColor:"#143d59", color: "white"}));
document.querySelector(".buttonFilter")?.addEventListener('mouseleave', () => gsap.to(".buttonFilter", {duration: 2,backgroundColor:"white",  color: "#143d59"}));

document.querySelector(".buttonFilter")?.addEventListener('click', getAllLikedRecipes);
document.querySelector(".DeleteInputFields")?.addEventListener('click', clearAllInput);

var signInBtn = document.querySelector("#SignInBtn") as HTMLButtonElement | null;
if(signInBtn != null) signInBtn.innerText = getCookie('name');
document.querySelector("#SignInBtn")?.addEventListener("click", openProfileForm);
document.querySelector("#signOutBtn")?.addEventListener("click", signOutAccount);
document.querySelector("#CloseProfile")?.addEventListener("click", closeProfileForm);



initializeApp(firebaseConfig);
getAllLikedRecipes();

function clearAllInput()
{
    let nameValue = document.getElementsByClassName("inputFilter")[0] as HTMLInputElement;
    if(nameValue != null) nameValue.value = "";

    let difficultyValue = document.getElementsByClassName("inputFilter")[1] as HTMLInputElement;
    if(difficultyValue != null) difficultyValue.value = "";

    let timeValue = document.getElementsByClassName("inputFilter")[2] as HTMLInputElement;
    if(timeValue != null) timeValue.value = "";
}

async function getAllLikedRecipes()
{
    const db = getDatabase();
    const storage = getStorage();

    let listOfLikedRecipesIDs : string[] = [];

    await onValue(ref(db, 'users/' + getCookie('id') + '/liked'), (liked) => {

        
        liked.forEach((likedRecipes) => { let id : string = likedRecipes.key!; listOfLikedRecipesIDs.push(id); });
        selectAllRecipes(listOfLikedRecipesIDs); 
     });     
}

async function selectAllRecipes(listOfLikedRecipesIDs : string[])
{
    const db = getDatabase();
    const storage = getStorage();

    let nameValue = document.getElementsByClassName("inputFilter")[0] as HTMLInputElement;
    let name = "";
    if(nameValue != null) name = String(nameValue.value);

    let difficultyValue = document.getElementsByClassName("inputFilter")[1] as HTMLInputElement;
    let difficulty = "";
    if(difficultyValue != null) difficulty = String(difficultyValue.value);

    let timeValue = document.getElementsByClassName("inputFilter")[2] as HTMLInputElement;
    let time = "";
    if(timeValue != null) time = String(timeValue.value);

    //var name = String(document.getElementsByClassName("inputFilter")[0].value);
    //var difficulty = String(document.getElementsByClassName("inputFilter")[1].value);
    //var time = String(document.getElementsByClassName("inputFilter")[2].value);

    let recipeList = document.querySelector('.recipeList')
    if(recipeList != null) recipeList.innerHTML = "";

    for(let i = 0; i < listOfLikedRecipesIDs.length; i++)
    {
       await onValue(ref(db, 'recipes/' + listOfLikedRecipesIDs[i]), (recipe) => {

        if(name.length > 0 || time.length > 0 || difficulty.length > 0)
        {
            if(name.length == 0) name = '/';
            if(time.length == 0) time = '/';
            if(difficulty.length == 0) difficulty = '/';
            if(recipe.val().title.includes(name) || recipe.val().CookingTime == (time + " min") || recipe.val().CookingDifficulty.includes(difficulty))
            {
                getDownloadURL(sRef(storage, 'recipeImages/' + recipe.val().Image)).then((url) => 
               { 
                   if(recipeList != null) { recipeList.innerHTML += 

                   `<recipe-short-intro image="${url}" id="${listOfLikedRecipesIDs[i]}">
                       <span id="RecipeName" slot="RecipeName">${recipe.val().title}</span>
                       <span id="ShortText" slot="ShortText">${recipe.val().shortText}</span>
                   </recipe-short-intro>` }
               }) 
            }
        }
        else
        {
            getDownloadURL(sRef(storage, 'recipeImages/' + recipe.val().Image)).then((url) => 
            { 
                if(recipeList != null) {recipeList.innerHTML += 

                `<recipe-short-intro image="${url}" id="${listOfLikedRecipesIDs[i]}">
                    <span id="RecipeName" slot="RecipeName">${recipe.val().title}</span>
                    <span id="ShortText" slot="ShortText">${recipe.val().shortText}</span>
                </recipe-short-intro>`}
            }) 
        }
        
    });      
    }

}


function getCookie(name : string) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ))
    return matches ? decodeURIComponent(matches[1]) : ""
}


function openProfileForm()
{
    onValue(ref(getDatabase(), `users/${getCookie('id')}`), (snapshot : any) => 
    {
        const nameBox = document.querySelector('#displayNameBox') as HTMLElement | null;
        if(nameBox !=null) nameBox.innerText = getCookie('name');

        const emailBox =document.getElementById('emailBox') as HTMLElement | null;
        if(emailBox !=null) emailBox.innerText = snapshot.val().email;
    })

    const profileForm = document.querySelector('#ProfileForm') as HTMLFormElement | null;
    if(profileForm !=null) profileForm.style.display ='block';
}

async function signOutAccount()
{
    getAuth().signOut();
    closeProfileForm();
    const btn = document.querySelector('#SignInBtn') as HTMLButtonElement | null;
    if(btn != null) btn.innerText ='Sign in';
    window.location.href = '../index.html';
}

function closeProfileForm()
{
    const profileForm = document.querySelector('#ProfileForm') as HTMLFormElement | null;
    if(profileForm != null) profileForm.style.display ='none';
}