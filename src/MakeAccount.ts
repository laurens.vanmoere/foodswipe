import { initializeApp } from 'firebase/app';
import { getAuth, createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword} from "firebase/auth";
import { getDatabase, ref, set } from "firebase/database";

const firebaseConfig = {
    apiKey: "AIzaSyAx_J5kpKeSgMjfZZ4M5_bdPcfp6KPl75E",
    authDomain: "foodswipe-35b2a.firebaseapp.com",
    databaseURL: "https://foodswipe-35b2a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "foodswipe-35b2a",
    storageBucket: "foodswipe-35b2a.appspot.com",
    messagingSenderId: "51231404668",
    appId: "1:51231404668:web:0ccdc35c8dff89a3a28ff9",
    measurementId: "G-X6MMWS4ZRV"
};

initializeApp(firebaseConfig);


document.querySelector("#MakeAccountBtn")?.addEventListener("click", openRegisterForm);
document.querySelector("#closeFrom")?.addEventListener("click", closeRegisterForm);

document.querySelector("#SignInBtn")?.addEventListener("click", openSignInForm);
document.querySelector("#CloseProfile")?.addEventListener("click", closeProfileForm);
document.querySelector("#CloseSignIn")?.addEventListener("click", closeSignInForm);

document.querySelector('#LoginBtn')?.addEventListener("click", makeAccount);
document.querySelector('#SignInBtnForm')?.addEventListener("click", signInAccount);

document.querySelector('#signOutBtn')?.addEventListener("click", signOutAccount);


async function makeAccount()
{
    const emailBox = document.getElementById('email') as HTMLInputElement | null;
    let email = "";
    if(emailBox != null) email = emailBox?.value;

    const password1Box = document.getElementById('password1') as HTMLInputElement | null;
    let password1 = "";
    if(password1Box != null) password1 = password1Box.value;

    const password2Box = document.getElementById('password2') as HTMLInputElement | null;
    let password2 = "";
    if(password2Box != null) password2 = password2Box.value;

    if(password1 == password2)
    {
        var auth = getAuth();
        await createUserWithEmailAndPassword(auth, email, password1).then(() => closeRegisterForm());
        auth = getAuth();
        if(auth.currentUser != null) {
            let nameBox = document.getElementById('name') as HTMLInputElement | null;
            let user = auth.currentUser;
            if(nameBox != null)
                await updateProfile(user, {displayName: nameBox.value});
        }

        auth = getAuth();
        var user = auth.currentUser;

        if(user != null){
            
            await user.providerData.forEach((profile) => {
                let signInBtn = document.getElementById('SignInBtn') as HTMLButtonElement | null;
                if(signInBtn != null) signInBtn.innerText = String(profile.displayName);
            });

            await set(ref(getDatabase(), 'users/' + user.uid.toString()), 
            { 
                email: user.email,
                recipeCount: { Count: 1 },
                name:  user.displayName
            });

            setCookie('id', user.uid.toString(), '/');
            setCookie('name', String(user.displayName), '/');

        } 

        if(getCookie('id') && getCookie('name')) 
        {
            window.location.href = '../swipePage.html';
        }
    }
    else
    {
        console.log("Pasword mismatch");
        // Passwords dont match
    }
}

async function signInAccount()
{
    const emailBox = document.getElementById('emailSignIn') as HTMLInputElement | null;
    let email = "";
    if(emailBox != null) email = emailBox?.value;

    const passwordBox = document.getElementById('passwordSignIn') as HTMLInputElement | null;
    let password = "";
    if(passwordBox != null) password = passwordBox.value;

    var auth = getAuth();
    await signInWithEmailAndPassword(auth, email, password);
    closeSignInForm();

    let user = auth.currentUser;
    let userId = "";
    let userName = "";
    if(user != null){
        userId = String(user.uid); 
        userName = String(user.displayName);
    } 
    setCookie('id', userId, '/');
    setCookie('name', userName, '/');

    if(getCookie('id') && getCookie('name')) 
    {
        window.location.href = '../swipePage.html';
    }
}

function setCookie(name : string, value : string, path : string, options = {}) {
    options = {
        path: path,
        ...options
    }; 
    /*if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    } */

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value)
    for (let optionKey in options) {
        updatedCookie += "; " + optionKey
        let optionValue = options[optionKey as keyof typeof options];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue
        }
    }
    document.cookie = updatedCookie;
}

function getCookie(name : string) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ))
    return matches ? decodeURIComponent(matches[1]) : ""
}


function openSignInForm()
{
    var auth = getAuth();
    const user = auth.currentUser;

    if(user == null)
    {
        const signinForm = document.querySelector('#SignInForm') as HTMLFormElement | null;
        if(signinForm != null) signinForm.style.display ='block';

        document.querySelectorAll("div:not(#SignInForm)").forEach(element => {
            if(element.className !== 'ColorStroke' && element.className !== 'loginLinks' && element.className !== 'links')
            {
                const elementvar = element as HTMLElement;
                if(elementvar != null) elementvar.style.filter = 'brightness(70%)';
            }
        });
    }
    else
    {
        const profileForm = document.querySelector('#ProfileForm') as HTMLFormElement | null;
        if(profileForm != null) profileForm.style.display ='block';

        const nameBox = document.querySelector('#displayNameBox') as HTMLElement | null;
        if(nameBox !=null) nameBox.innerText = String(user.displayName);
        const emailBox =document.getElementById('emailBox') as HTMLElement | null;
        if(emailBox !=null) emailBox.innerText = String(user.email);

        document.querySelectorAll("div:not(#ProfileForm)").forEach(element => {
            if(element.className !== 'ColorStroke' && element.className !== 'loginLinks' && element.className !== 'links')
            {
                const elementvar = element as HTMLElement;
                if(elementvar != null) elementvar.style.filter = 'brightness(70%)';
            }
        });
    }

}


async function signOutAccount()
{
    getAuth().signOut();
    closeProfileForm();
    const btn = document.querySelector('#SignInBtn') as HTMLButtonElement | null;
    if(btn != null) btn.innerText ='Sign in';
}


function closeProfileForm()
{
    const profileForm = document.querySelector('#ProfileForm') as HTMLFormElement | null;
    if(profileForm != null) profileForm.style.display ='none';
    document.querySelectorAll("div").forEach(element => {
        element.style.filter = 'brightness(100%)';
    });
}

function closeSignInForm()
{
    const signinForm = document.querySelector('#SignInForm') as HTMLFormElement | null;
    if(signinForm != null) signinForm.style.display ='none';
    document.querySelectorAll("div").forEach(element => {
        element.style.filter = 'brightness(100%)';
    });
}


function openRegisterForm()
{
    const registerForm = document.querySelector('#RegisterForm') as HTMLFormElement | null;
    if(registerForm != null) registerForm.style.display ='block';
    
    document.querySelectorAll("div:not(#RegisterForm)").forEach((element) => {
        if(element.className !== 'ColorStroke' && element.className !== 'loginLinks' && element.className !== 'links')
        {
            const elementvar = element as HTMLElement;
            if(elementvar != null) elementvar.style.filter = 'brightness(70%)';
        }
    });
}

function closeRegisterForm()
{
    const registerForm = document.querySelector('#RegisterForm') as HTMLFormElement | null;
    if(registerForm != null) registerForm.style.display ='none';
    document.querySelectorAll("div").forEach(element => {
        element.style.filter = 'brightness(100%)';
    });
}