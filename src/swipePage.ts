import { initializeApp } from 'firebase/app';
import { getDatabase, get, ref, set, onValue, push, DataSnapshot } from "firebase/database";
import { ref as sRef, getDownloadURL, getStorage  } from 'firebase/storage';
import { getAuth, createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyAx_J5kpKeSgMjfZZ4M5_bdPcfp6KPl75E",
    authDomain: "foodswipe-35b2a.firebaseapp.com",
    databaseURL: "https://foodswipe-35b2a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "foodswipe-35b2a",
    storageBucket: "foodswipe-35b2a.appspot.com",
    messagingSenderId: "51231404668",
    appId: "1:51231404668:web:0ccdc35c8dff89a3a28ff9",
    measurementId: "G-X6MMWS4ZRV"
};

initializeApp(firebaseConfig);
var recipeCounter = 1;

document.querySelector("#AddBtn")?.addEventListener("click", AddRecipe);
document.querySelector("#ReturnBtn")?.addEventListener("click", getLastRecipe);
document.querySelector("#ThrowAwayBtn")?.addEventListener("click", ThrowAwayRecipe);

var signInBtn = document.querySelector("#SignInBtn") as HTMLButtonElement | null;
if(signInBtn != null) signInBtn.innerText = getCookie('name');
document.querySelector("#SignInBtn")?.addEventListener("click", openProfileForm);
document.querySelector("#signOutBtn")?.addEventListener("click", signOutAccount);
document.querySelector("#CloseProfile")?.addEventListener("click", closeProfileForm);

let userID = getCookie('id');
getRecipe();


function getCookie(name : string) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ))
    return matches ? decodeURIComponent(matches[1]) : ""
}

async function ThrowAwayRecipe()
{
    set(ref(getDatabase(), 'users/' + userID + '/recipeCount/'), 
    { 
        Count: recipeCounter + 1
    });

    recipeCounter++;

    getRecipe();
    
}

async function getRecipe()
{
    let recipeCount = 1;
       
    await onValue(ref(getDatabase(), `users/` + userID), (snapshot : any) => {
        recipeCount = snapshot.val().recipeCount['Count'];
        getRecipeByNumber(recipeCount);
    });
}

async function getRecipeByNumber(recipeNumber : number)
{
    const title = document.getElementById('Title') as HTMLElement | null;
    const shortText = document.getElementById('ShortText') as HTMLElement | null;
    const cookingDifficulty = document.getElementById('CookingDifficulty') as HTMLElement | null;
    const cookingTime = document.getElementById('CookingTime') as HTMLElement | null;
    const cookingServes = document.getElementById('CookingServes') as HTMLElement | null;

    await onValue(ref(getDatabase(), `recipes/${recipeNumber}`), (snapshot : any) => {
        if(title != null) title.innerText = snapshot.val().title;
        if(shortText != null) shortText.innerText = snapshot.val().shortText;
        if(cookingDifficulty != null) cookingDifficulty.innerText = snapshot.val().CookingDifficulty;
        if(cookingTime != null) cookingTime.innerText = snapshot.val().CookingTime;
        if(cookingServes != null) cookingServes.innerText = snapshot.val().CookingServes;
        const storage = getStorage();

        getDownloadURL(sRef(storage, 'recipeImages/' + snapshot.val().Image)).then((url : any) => {
            document.querySelector('recipe-intro')?.setAttribute('image', url);
        }); 
    });
}

async function getLastRecipe()
{
    await set(ref(getDatabase(), 'users/' + userID + '/recipeCount/'), 
    { 
        Count: recipeCounter - 1
    });
    getRecipe();
    var auth = getAuth();
    const user = auth.currentUser;
}

function openProfileForm()
{
    onValue(ref(getDatabase(), `users/${getCookie('id')}`), (snapshot : any) => 
    {
        const nameBox = document.querySelector('#displayNameBox') as HTMLElement | null;
        if(nameBox !=null) nameBox.innerText = getCookie('name');

        const emailBox =document.getElementById('emailBox') as HTMLElement | null;
        if(emailBox !=null) emailBox.innerText = snapshot.val().email;
    })

    const profileForm = document.querySelector('#ProfileForm') as HTMLFormElement | null;
    if(profileForm !=null) profileForm.style.display ='block';
}

async function signOutAccount()
{
    getAuth().signOut();
    closeProfileForm();
    const btn = document.querySelector('#SignInBtn') as HTMLButtonElement | null;
    if(btn != null) btn.innerText ='Sign in';
    window.location.href = '../index.html';
}

function closeProfileForm()
{
    const profileForm = document.querySelector('#ProfileForm') as HTMLFormElement | null;
    if(profileForm != null) profileForm.style.display ='none';
}

async function AddRecipe()
{
    const db = getDatabase();
    await set(ref(db, 'users/' + getCookie('id') + '/liked/' + recipeCounter), 
    { 
        dateAdded: new Date().toLocaleString()
    });
    
    
    set(ref(getDatabase(), 'users/' + userID + '/recipeCount/'), 
    { 
        Count: recipeCounter + 1
    });

    CheckLikedRecipesOtherPeople();

    recipeCounter++;

    getRecipe();
    
    
}

async function CheckLikedRecipesOtherPeople()
{
    if(recipeCounter === 5)
    {
        await AddFriends(); 
    }
}

async function AddFriends()
{
    const db = getDatabase();
    const storage = getStorage();

    console.log("helllloooo");

    let listOf3Users : string[] = [];

    await onValue(ref(db, 'users/'), (AllUsers : any) => {

        let friendCounter = 0;
        AllUsers.forEach((user : any) => 
        {   
            if(friendCounter < 3)
            {
                if(user.key != getCookie('id'))
                {   
                    let id : string = user.key;
                    listOf3Users.push(id);
                    friendCounter++;
                }
            }

            if(listOf3Users.length == 3)
            {
                for(let i = 0; i < listOf3Users.length; i++)
                {
                    set(ref(getDatabase(), 'users/' + getCookie('id') + '/friends/' + listOf3Users[i]),
                    {
                        TimeAdded : time
                    })   
                }
            }
        }); 
    });   

    const d = new Date();
    let time = d.getTime();
    

}
