

const template = document.createElement('template')

template.innerHTML = `
<style>

.Card {
    margin: auto;
    width: 40%;
    background-color:var(--main-dark-color);

    color: white;

    border-radius: 15px;
    border: 3px solid var(--main-dark-color);
}

.Cardhalf1
{
    padding: 2px;;
}

.Cardhalf1 img{
    width: 100%;
    height: 350px;
    object-fit: cover;
    border-radius: 10px;
}

h2 {
    text-align: center;
    font-size: x-large;
}

.Toppart{
    text-align: left;
}

.CookingInfoBlocks {
    display: flex;
    justify-content: center;
}

.CookingInfoBlock {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 30%;
    height: 50px;
    margin: 5px;
    border-bottom: 2px solid var(--main-orange-color);
}

.CookingInfoBlock img{
    width: 40px;
    height: 40px;

    margin: 2px;
    margin-right: 5px;
}
</style>

<div class="Card">
    <div class="CardHalf1">
        <img src="">
    </div>

    <div class="CardHalf2">
        <h2><slot name="Title"/>Title</h2>
        <p><slot name="ShortText"/>ShortText</p>
            
        <div class="CookingInfoBlocks">
            <div class="CookingInfoBlock">
                <img src="../images/TimeIcon.png">
                <p><slot name="CookingTime"/></p>
            </div>

            <div class="CookingInfoBlock">
                <img id="CookingDifficultyImage" src="../images/CookingDifficulty.png">
                <p><slot name="CookingDifficulty"/></p>
            </div>

            <div class="CookingInfoBlock">
                <img src="../images/Serves.png">
                <p><slot name="CookingServes"/></p>
            </div>
        </div>
    </div>
</div>
`

class RecipeIntro extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode:'open'});
        if(this.shadowRoot != null) this.shadowRoot.appendChild(template.content.cloneNode(true));
    }

    static get observedAttributes() {
        return ['image'];
      }

    attributeChangedCallback()
    { 
        if(this.shadowRoot != null)
        {
            const image = this.shadowRoot.querySelector('img');
            const imageAttr = this.getAttribute('image')
            if(image != null && image.src != null && imageAttr != null) { image.src = imageAttr; } 
        }
    }
}

customElements.define('recipe-intro', RecipeIntro);

