
const friendsTemplate = document.createElement('template')

friendsTemplate.innerHTML = `
<style>
.listElement
{
    display:flex; 
    width:60%; 
    justify-content: space-between;
    align-items: center; 
    margin: auto; 
    text-align: left; 
    border: 3px solid var(--main-dark-color); 
    border-radius: 5px;

    margin-bottom: 10px;
}

img
{
    width: 150px; 
    height: 150px; 
    margin: 10px; 
    border-radius: 5px;
}

button
{
    border-radius: 30px;
    height: 40px;
    background-color: var(--main-dark-color); 
    padding: 5px;

    width: 150px;
    cursor: pointer;
    border: none;
    color:white;
    margin: 20px;;
}
</style>

<div class="listElement">
    <img>
    <div style="margin: 5px;">
        <h2><slot name="PersonName"/>PersonName</h2>
    </div>

    <button>View Profile</button>
</div>
`

class FriendsIntro extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode:'open'});
        if(this.shadowRoot != null) this.shadowRoot.appendChild(friendsTemplate.content.cloneNode(true));
    }

    static get observedAttributes() {
        return ['image'];
      }

    attributeChangedCallback()
    {
        //this.shadowRoot.querySelector('img').src = this.getAttribute('image');

        if(this.shadowRoot != null)
        {
            const image = this.shadowRoot.querySelector('img');
            const imageAttr = this.getAttribute('image')
            if(image != null && image.src != null && imageAttr != null) { image.src = imageAttr; } 
        }
    }
}


customElements.define('friends-intro', FriendsIntro);