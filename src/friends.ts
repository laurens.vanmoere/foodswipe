import { initializeApp } from 'firebase/app';
import { getDatabase, ref, query, onValue } from "firebase/database";
import { ref as sRef, getDownloadURL, getStorage, list  } from 'firebase/storage';
import { getAuth, createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyAx_J5kpKeSgMjfZZ4M5_bdPcfp6KPl75E",
    authDomain: "foodswipe-35b2a.firebaseapp.com",
    databaseURL: "https://foodswipe-35b2a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "foodswipe-35b2a",
    storageBucket: "foodswipe-35b2a.appspot.com",
    messagingSenderId: "51231404668",
    appId: "1:51231404668:web:0ccdc35c8dff89a3a28ff9",
    measurementId: "G-X6MMWS4ZRV"
};


var signInBtn = document.querySelector("#SignInBtn") as HTMLButtonElement | null;
if(signInBtn != null) signInBtn.innerText = getCookie('name');
document.querySelector("#SignInBtn")?.addEventListener("click", openProfileForm);
document.querySelector("#signOutBtn")?.addEventListener("click", signOutAccount);
document.querySelector("#CloseProfile")?.addEventListener("click", closeProfileForm);


initializeApp(firebaseConfig);
getFriends();


async function getFriends()
{
    const db = getDatabase();
    const storage = getStorage();

    let listOfLikedRecipesIDs : string[] = [];

    const friendsList = document.querySelector('.friendsList') as HTMLElement | null;
    if(friendsList != null) friendsList.innerHTML = "";

    await onValue(ref(db, 'users/' + getCookie('id') + '/friends'), (friends) => {
        friends.forEach((friend) => { 
            onValue(ref(db, 'users/' + friend.key), (user) => {
                if(friendsList != null)
                {
                    friendsList.innerHTML += 
                    `<friends-intro image="../images/profileDummy.png">
                        <span id="PersonName" slot="PersonName">${user.val().name}</span>
                    </friends-intro>`
                }
            })
        }); 
     }); 
}

function getCookie(name : string) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ))
    return matches ? decodeURIComponent(matches[1]) : ""
}

function openProfileForm()
{
    onValue(ref(getDatabase(), `users/${getCookie('id')}`), (snapshot : any) => 
    {
        const nameBox = document.querySelector('#displayNameBox') as HTMLElement | null;
        if(nameBox !=null) nameBox.innerText = getCookie('name');

        const emailBox =document.getElementById('emailBox') as HTMLElement | null;
        if(emailBox !=null) emailBox.innerText = snapshot.val().email;
    })

    const profileForm = document.querySelector('#ProfileForm') as HTMLFormElement | null;
    if(profileForm !=null) profileForm.style.display ='block';
}

async function signOutAccount()
{
    getAuth().signOut();
    closeProfileForm();
    const btn = document.querySelector('#SignInBtn') as HTMLButtonElement | null;
    if(btn != null) btn.innerText ='Sign in';
    window.location.href = '../index.html';
}

function closeProfileForm()
{
    const profileForm = document.querySelector('#ProfileForm') as HTMLFormElement | null;
    if(profileForm != null) profileForm.style.display ='none';
}