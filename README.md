# WebtopicsProjectFoodSwipe



## Getting started

Dit is een webproject gemaakt voor het vak web-topics. Deze webiste maakt het mogelijk om gerechten te liken of disliken en op basis hiervan mensen te leren kennen. (Let op er zitten nu enkel 6 gerechten in de database)

### Web onderdelen
Er is gebruik gemaakt van volgende web topics:<br />
    - **typescript**<br />
    - **RSS-Feed**<br />
    - **webpack**<br />
    - **Web components**: Om vrienden, gerechten kort en lang voor te stellen<br />
    - **web animations**: (gsap) Om de website wat interactief te maken<br />
    - **firebase realtime database/authentication**: Voor users en gerechten<br />
    - **firebase hosting**: om de website te hosten<br />
    - **firebase Storage**: Om afbeeldingen van gerechten op te slaan.<br />

### Webpagina's

Er zijn 4 subpagina's:<br />
    - **Swipe**: Hier kan u gerechten liken of disliken.<br />
    - **Recipes**: Hier komen de gerechten dat u geliked heeft, u kan hier ook filteren op gerechten op basis van tijd, naam           of moeilijkheid.<br />
    - **Friends**: Hier ziet u uw vrieden die dezelfde smaak in gerechten hebben.<br />
    - **RSS**: Alle recepten komen van BBC Cooking. Als dank om deze gerechten te mogen gebruiken implementeer ik een rss feed van de artikels op hun website.<br />


